<?php

namespace Drupal\rh_editionguard_book\Plugin\RabbitHoleEntityPlugin;

use Drupal\rabbit_hole\Plugin\RabbitHoleEntityPluginBase;

/**
 * Implements rabbit hole behavior for nodes.
 *
 * @RabbitHoleEntityPlugin(
 *  id = "rh_editionguard_book",
 *  label = @Translation("EditionGuard book"),
 *  entityType = "editionguard_book"
 * )
 */
class EditionGuardBook extends RabbitHoleEntityPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getGlobalConfigFormId() {
    return "editionguard_book_settings";
  }

}
