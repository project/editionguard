CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

 * For a full description of the module visit:
   https://www.drupal.org/project/editionguard

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/editionguard


REQUIREMENTS
------------

You will need to have the
<a href="https://www.drupal.org/project/editionguard">EditionGuard</a>
module installed and configured.


INSTALLATION
------------

Install the EditionGuard module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.


CONFIGURATION
-------------

1. Navigate to Administration > Extend and enable the module.
2. Navigate to Administration > Structure > EditionGuard books >
EditionGuard book settings Add a book prefix (Optional).

This module provides 2 entities (EditionGuard book and EditionGuard transaction)
and a custom field (EditionGuard field).

USAGE
-------------
1. Navigate to Administration > Structure > EditionGuard books
2. Click on Add EditionGuard book
3. Add a book
4. Navigate to Administration > Structure > EditionGuard books >
Create Transaction
5. Add a transaction

This process can be automated but requires a bit of coding.
You can view the method used in https://www.drupal.org/project/commerce_editionguard
to bridge customer orders with EditionGuard books by creating programmatically
new transactions and making them available to the user end.

Check src/Form/TransactionCreateForm.php for a example usage of the API.

IMPORTANT NOTES
---------------
The books are saved into the sites private folder 'EditionGuard books'.

The custom reference field EditionGuard book is not visible on Front End
of the website.
You should still disable the field from Manage Display Tab.

If you need to show the books use Views instead.

MAINTAINERS
-----------

 * lexsoft - https://www.drupal.org/u/lexsoft
