<?php

namespace Drupal\editionguard\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;

/**
 * Plugin implementation of the 'editionguard_book_field_default_widget' widget.
 *
 * @FieldWidget(
 *   id = "editionguard_book_field_default_widget",
 *   module = "editionguard",
 *   label = @Translation("EditionGuard book field default widget"),
 *   field_types = {
 *     "editionguard_book_field"
 *   }
 * )
 */
class EdtionGuardBookFieldDefaultWidget extends EntityReferenceAutocompleteWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $item = $items[$delta];
    $values = $item->getValue();
    $field_name = $item->getFieldDefinition()->getName();
    $field_name = str_replace('_', '-', $field_name);

    $element += [
      '#type' => 'details',
      '#title' => $this->t('EditionGuard Book'),
      '#collapsible' => TRUE,
      '#open' => TRUE,
    ];

    $element['show_instructions'] = [
      '#type' => 'select',
      '#title' => $this->t('Show Instructions'),
      '#default_value' => $values['show_instructions'] ?? 'true',
      '#options' => [
        1 => $this->t('Yes'),
        0 => $this->t('No'),
      ],
    ];

    $element['uses_remaining'] = [
      '#type' => 'number',
      '#title' => $this->t('Uses remaining'),
      '#default_value' => $values['uses_remaining'] ?? NULL,
    ];

    $element['watermark_enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Watermark - only works on books with DRM EditionMark'),
      '#description' => $this->t('Whether Watermark is allowed on this ebook. Customer name and email will be used.'),
      '#default_value' => $values['watermark_enable'] ?? 0,
    ];

    $element['watermark_place_begin'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Place Watermark at start of book'),
      '#default_value' => $values['watermark_place_begin'] ?? 0,
      '#states' => [
        'visible' => [
          ':input[id="edit-' . $field_name . '-' . $delta . '-watermark-enable"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $element['watermark_place_end'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Place Watermark at end of book'),
      '#default_value' => $values['watermark_place_end'] ?? 0,
      '#states' => [
        'visible' => [
          ':input[id="edit-' . $field_name . '-' . $delta . '-watermark-enable"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $element['watermark_place_random'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Place Watermark in random location in book'),
      '#default_value' => $values['watermark_place_random'] ?? 0,
      '#states' => [
        'visible' => [
          ':input[id="edit-' . $field_name . '-' . $delta . '-watermark-enable"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $element['watermark_place_random_count'] = [
      '#type' => 'number',
      '#title' => $this->t('Place Watermark in X number of random locations.'),
      '#default_value' => $values['watermark_place_random_count'] ?? 0,
      '#states' => [
        'visible' => [
          ':input[id="edit-' . $field_name . '-' . $delta . '-watermark-enable"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return $element;
  }

}
