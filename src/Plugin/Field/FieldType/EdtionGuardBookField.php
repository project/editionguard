<?php

namespace Drupal\editionguard\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'editionguard_book_field' field type.
 *
 * @FieldType(
 *   id = "editionguard_book_field",
 *   label = @Translation("EditionGuard book"),
 *   description = @Translation("Display EditionGuard available books"),
 *   category = @Translation("Reference"),
 *   default_widget = "editionguard_book_field_default_widget",
 *   default_formatter = "editionguard_book_field_default_formatter",
 *   list_class = "\Drupal\Core\Field\EntityReferenceFieldItemList",
 * )
 */
class EdtionGuardBookField extends EntityReferenceItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'target_type' => 'editionguard_book',
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Prevent early t() calls by using the TranslatableMarkup.
    $properties = parent::propertyDefinitions($field_definition);

    $properties['show_instructions'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Show Instructions'))
      ->setDescription(t('Whether show instructions is allowed on this ebook.'));

    $properties['uses_remaining'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Uses Remaining'));

    $properties['watermark_enable'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Whether Watermark is allowed on this ebook.'));

    $properties['watermark_place_begin'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Place Watermark at start of book.'));

    $properties['watermark_place_end'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Place Watermark at end of book.'));

    $properties['watermark_place_random'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Place Watermark in random location in book.'));

    $properties['watermark_place_random_count'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Place Watermark in X number of random locations.'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);
    $schema['columns']['uses_remaining'] = [
      'description' => 'Uses remaining.',
      'type' => 'int',
      'unsigned' => TRUE,
    ];
    $schema['columns']['show_instructions'] = [
      'description' => 'Can be blank, true or false.',
      'type' => 'int',
      'unsigned' => TRUE,
    ];
    $schema['columns']['watermark_enable'] = [
      'description' => 'Whether Watermark is allowed on this ebook: 0 = no, 1 = yes.',
      'type' => 'int',
      'default' => 0,
    ];
    $schema['columns']['watermark_place_begin'] = [
      'description' => 'Place Watermark at start of book: 0 = no, 1 = yes.',
      'type' => 'int',
      'default' => 0,
    ];
    $schema['columns']['watermark_place_end'] = [
      'description' => 'Place Watermark at end of book: 0 = no, 1 = yes.',
      'type' => 'int',
      'default' => 0,
    ];
    $schema['columns']['watermark_place_random'] = [
      'description' => 'Place Watermark in random location in book: 0 = no, 1 = yes.',
      'type' => 'int',
      'default' => 0,
    ];
    $schema['columns']['watermark_place_random_count'] = [
      'description' => 'Place Watermark in X number of random locations.',
      'type' => 'int',
      'default' => 0,
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public static function getPreconfiguredOptions() {
    return [];
  }

}
