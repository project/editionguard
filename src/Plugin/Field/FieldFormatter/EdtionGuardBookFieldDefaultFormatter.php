<?php

namespace Drupal\editionguard\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceLabelFormatter;

/**
 * Plugin implementation of the 'editionguard_book_field_default_formatter'.
 *
 * @FieldFormatter(
 *   id = "editionguard_book_field_default_formatter",
 *   label = @Translation("EditionGuard book field default formatter"),
 *   field_types = {
 *     "editionguard_book_field"
 *   }
 * )
 */
class EdtionGuardBookFieldDefaultFormatter extends EntityReferenceLabelFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // Does not actually output anything.
    return [];
  }

}
