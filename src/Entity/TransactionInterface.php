<?php

namespace Drupal\editionguard\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface for defining EditionGuard Transaction entities.
 *
 * @ingroup editionguard
 */
interface TransactionInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the EditionGuard Transaction name.
   *
   * @return string
   *   Name of the EditionGuard Transaction.
   */
  public function getName();

  /**
   * Sets the EditionGuard Transaction name.
   *
   * @param string $name
   *   The EditionGuard Transaction name.
   *
   * @return \Drupal\editionguard\Entity\TransactionInterface
   *   The called EditionGuard Transaction entity.
   */
  public function setName($name);

  /**
   * Gets the EditionGuard Transaction creation timestamp.
   *
   * @return int
   *   Creation timestamp of the EditionGuard Transaction.
   */
  public function getCreatedTime();

  /**
   * Sets the EditionGuard Transaction creation timestamp.
   *
   * @param int $timestamp
   *   The EditionGuard Transaction creation timestamp.
   *
   * @return \Drupal\editionguard\Entity\TransactionInterface
   *   The called EditionGuard Transaction entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the book ID.
   *
   * @return int
   *   The EditionGuard book ID.
   */
  public function getBookId();

  /**
   * Gets the EditionGuard Transaction external ID.
   *
   * @return string
   *   External ID of the EditionGuard Transaction.
   */
  public function getExternalId();

  /**
   * Gets the EditionGuard Transaction ID.
   *
   * @return string
   *   ID of the EditionGuard Transaction.
   */
  public function getTransactionId();

  /**
   * Indicates if instructions are showed.
   *
   * @return bool
   *   TRUE if set to show instructions, FALSE otherwise.
   */
  public function getShowInstructions();

  /**
   * The initial number set for uses remaining.
   *
   * @return int
   *   Initial number.
   */
  public function getUsesRemaining();

  /**
   * Indicates if transaction is fulfilled.
   *
   * @return bool
   *   TRUE if the transaction is fulfilled, FALSE otherwise.
   */
  public function getIsFulfilled();

  /**
   * Indicates if the book contains watermark.
   *
   * @return bool
   *   TRUE if the watermark is enabled, FALSE otherwise.
   */
  public function getWatermark();

}
