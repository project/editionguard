<?php

namespace Drupal\editionguard\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the EditionGuard book entity.
 *
 * @ingroup editionguard
 *
 * @ContentEntityType(
 *   id = "editionguard_book",
 *   label = @Translation("EditionGuard book"),
 *   label_collection = @Translation("EditionGuard books"),
 *   label_singular = @Translation("editionguard book"),
 *   label_plural = @Translation("editionguard books"),
 *   label_count = @PluralTranslation(
 *     singular = "@count EditionGuard book",
 *     plural = "@count EditionGuard books",
 *   ),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\editionguard\BookListBuilder",
 *     "views_data" = "Drupal\editionguard\Entity\BookViewsData",
 *     "translation" = "Drupal\editionguard\BookTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\editionguard\Form\BookForm",
 *       "add" = "Drupal\editionguard\Form\BookForm",
 *       "edit" = "Drupal\editionguard\Form\BookForm",
 *       "delete" = "Drupal\editionguard\Form\BookDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\editionguard\BookHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\editionguard\BookAccessControlHandler",
 *   },
 *   base_table = "editionguard_book",
 *   data_table = "editionguard_book_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer editionguard_book",
 *   entity_keys = {
 *     "id" = "book_id",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/editionguard_book/{editionguard_book}",
 *     "add-form" = "/admin/structure/editionguard_book/add",
 *     "edit-form" = "/admin/structure/editionguard_book/{editionguard_book}/edit",
 *     "delete-form" = "/admin/structure/editionguard_book/{editionguard_book}/delete",
 *     "collection" = "/admin/structure/editionguard_book",
 *   },
 *   field_ui_base_route = "editionguard_book.settings"
 * )
 */
class Book extends ContentEntityBase implements BookInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('title', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getBookId() {
    return $this->get('editionguard_book_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getDrmType() {
    return $this->get('drm')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getResourceId() {
    return $this->get('editionguard_resource_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the EditionGuard book entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The name of the EditionGuard book.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['author'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Book author'))
      ->setDescription(t('Book author.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['publisher'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Book publisher'))
      ->setDescription(t('Book publisher.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['editionguard_book_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('EditionGuard book ID'))
      ->setDescription(t('The EditionGuard book ID from EditionGuard API.'))
      ->setReadOnly(TRUE)
      ->setSetting('unsigned', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['editionguard_resource_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('The EditionGuard book UUID'))
      ->setDescription(t('The EditionGuard book UUID from EditionGuard API.'))
      ->setReadOnly(TRUE)
      ->setSetting('unsigned', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['drm'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('DRM Type'))
      ->setDescription(t('Possible values: 1 Adobe (Legacy), 2 for Adobe (Hardened), 3 for EditionMark, 4 for EditionLink.'))
      ->setRequired(TRUE)
      ->setSetting('allowed_values', [
        '1' => t('Adobe (Legacy)'),
        '2' => t('Adobe (Hardened)'),
        '3' => t('EditionMark'),
        '4' => t('EditionLink'),
      ])
      ->setDefaultValue('2')
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['resource'] = BaseFieldDefinition::create('file')
      ->setLabel(t('DRM Books Upload'))
      ->setDescription(t('The book to upload to EdigtionGuard.'))
      ->setDefaultValue('')
      ->setSettings([
        'uri_scheme' => _editionguard_file_default_scheme(),
        'file_directory' => 'EditionGuard books',
        'file_extensions' => 'pdf epub',
        'description_field' => 1,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'file',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'file_generic',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the EditionGuard book is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
