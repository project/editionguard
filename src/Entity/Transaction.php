<?php

namespace Drupal\editionguard\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\link\LinkItemInterface;
use Drupal\user\UserInterface;

/**
 * Defines the EditionGuard Transaction entity.
 *
 * @ingroup editionguard
 *
 * @ContentEntityType(
 *   id = "editionguard_transaction",
 *   label = @Translation("EditionGuard Transaction"),
 *   label = @Translation("EditionGuard Transaction"),
 *   label_collection = @Translation("EditionGuard Transactions"),
 *   label_singular = @Translation("editionguard transaction"),
 *   label_plural = @Translation("editionguard transactions"),
 *   label_count = @PluralTranslation(
 *     singular = "@count editionguard transaction",
 *     plural = "@count editionguard transactions",
 *     context = "EditionGuard",
 *   ),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\editionguard\TransactionListBuilder",
 *     "views_data" = "Drupal\editionguard\Entity\TransactionViewsData",
 *
 *     "access" = "Drupal\editionguard\TransactionAccessControlHandler",
 *     "permission_provider" = "Drupal\editionguard\TransactionPermissionProvider",
 *
 *     "route_provider" = {
 *       "default" = "Drupal\editionguard\TransactionRouteProvider",
 *       "delete-multiple" = "Drupal\entity\Routing\DeleteMultipleRouteProvider",
 *     },
 *   },
 *   base_table = "editionguard_transaction",
 *   translatable = FALSE,
 *   admin_permission = "administer editionguard_transaction",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *   },
 *   links = {
 *     "collection" = "/admin/structure/editionguard_book/transactions",
 *   },
 * )
 */
class Transaction extends ContentEntityBase implements TransactionInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getBookId() {
    return $this->get('book_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalId() {
    return $this->get('external_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getTransactionId() {
    return $this->get('transaction_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getShowInstructions() {
    return $this->get('show_instructions')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getUsesRemaining() {
    return $this->get('uses_remaining')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getIsFulfilled() {
    return $this->get('is_fulfilled')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getWatermark() {
    return $this->get('watermark')->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User ID'))
      ->setDescription(t('The ID of the associated user.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default');

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the EditionGuard Transaction.'))
      ->setDefaultValue('')
      ->setRequired(TRUE);

    $fields['book_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('EditionGuard book'))
      ->setDescription(t('The EditionGuard book.'));

    $fields['transaction_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Transaction ID'))
      ->setDescription(t('The transaction ID from EditionGuard API.'))
      ->setReadOnly(TRUE);

    $fields['resource_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Resource UUID'))
      ->setDescription(t('The EditionGuard book UUID from EditionGuard API.'))
      ->setReadOnly(TRUE);

    $fields['external_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('External ID'))
      ->setDescription(t('A drupal ID associated with this transaction.'))
      ->setReadOnly(TRUE);

    $fields['link'] = BaseFieldDefinition::create('link')
      ->setLabel(t('Download Link'))
      ->setDescription(t('EditionGuard book transaction download link from EditionGuard API.'))
      ->setRequired(TRUE)
      ->setSettings([
        'link_type' => LinkItemInterface::LINK_EXTERNAL,
        'title' => DRUPAL_DISABLED,
      ]);

    $fields['show_instructions'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Show Instructions'))
      ->setDescription(t('A boolean indicating whether instructions are showed from EditionGuard API.'))
      ->setDefaultValue(FALSE);

    $fields['uses_remaining'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Uses remaining'))
      ->setDescription(t('The initial number set for uses remaining.'));

    $fields['watermark'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Watermark'))
      ->setDescription(t('A boolean indicating whether watermarks are enabled.'))
      ->setDefaultValue(FALSE);

    $fields['is_fulfilled'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Show Instructions'))
      ->setDescription(t('A boolean indicating whether the transaction is fulfilled.'))
      ->setDefaultValue(FALSE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the transaction was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the transaction was last edited.'));

    return $fields;
  }

}
