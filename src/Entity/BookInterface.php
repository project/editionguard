<?php

namespace Drupal\editionguard\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining EditionGuard book entities.
 *
 * @ingroup editionguard
 */
interface BookInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the EditionGuard book name.
   *
   * @return string
   *   Name of the EditionGuard book.
   */
  public function getName();

  /**
   * Sets the EditionGuard book name.
   *
   * @param string $name
   *   The EditionGuard book name.
   *
   * @return \Drupal\editionguard\Entity\BookInterface
   *   The called EditionGuard book entity.
   */
  public function setName($name);

  /**
   * Gets the EditionGuard book creation timestamp.
   *
   * @return int
   *   Creation timestamp of the EditionGuard book.
   */
  public function getCreatedTime();

  /**
   * Sets the EditionGuard book creation timestamp.
   *
   * @param int $timestamp
   *   The EditionGuard book creation timestamp.
   *
   * @return \Drupal\editionguard\Entity\BookInterface
   *   The called EditionGuard book entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the EditionGuard book ID.
   *
   * @return int
   *   Id of the EditionGuard book.
   */
  public function getBookId();

  /**
   * Gets the EditionGuard book DRM Type.
   *
   * @return int
   *   Id of the EditionGuard book.
   */
  public function getDrmType();

  /**
   * Gets the EditionGuard book resource ID.
   *
   * @return string
   *   Id of the EditionGuard book resource.
   */
  public function getResourceId();

}
