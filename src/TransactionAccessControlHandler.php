<?php

namespace Drupal\editionguard;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the EditionGuard Transaction entity.
 *
 * @see \Drupal\editionguard\Entity\Transaction.
 */
class TransactionAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\editionguard\Entity\TransactionInterface $entity */
    switch ($operation) {

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'change book editionguard_transaction');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete editionguard_book');
    }

    $account = $this->prepareUser($account);

    /** @var \Drupal\Core\Access\AccessResult $result */
    $result = parent::checkAccess($entity, $operation, $account);

    /** @var \Drupal\editionguard\Entity\TransactionInterface $entity */
    if ($result->isNeutral() && $operation == 'view') {
      if ($account->isAuthenticated() && $account->id() == $entity->getOwnerId()) {
        $result = AccessResult::allowedIfHasPermissions($account, ['view own editionguard_transaction']);
        $result = $result->cachePerUser()->addCacheableDependency($entity);
      }
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

}
