<?php

namespace Drupal\editionguard;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;

/**
 * Provides routes for the Order entity.
 */
class TransactionRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    $collection->addRequirements([
      '_permission' => 'administer editionguard_transaction',
    ]);

    return $collection;
  }

}
