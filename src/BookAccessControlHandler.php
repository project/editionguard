<?php

namespace Drupal\editionguard;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the EditionGuard book entity.
 *
 * @see \Drupal\editionguard\Entity\Book.
 */
class BookAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\editionguard\Entity\BookInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished editionguard_book');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published editionguard_book');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit editionguard_book');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete editionguard_book');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add editionguard_book');
  }

}
