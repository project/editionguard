<?php

namespace Drupal\editionguard\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\editionguard\Entity\TransactionInterface;
use Drupal\editionguard_api\EditionGuardApiClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Manually allocate a different book to an EditionGuard Transaction.
 *
 * @internal
 */
class TransactionChangeBookForm extends FormBase {

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The EditionGuard API Client.
   *
   * @var \Drupal\editionguard_api\EditionGuardApiClientInterface
   */
  protected $editionGuard;

  /**
   * Constructs a new EditionGuard object.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\editionguard_api\EditionGuardApiClientInterface $editionguard_api
   *   The EditionGuard client api.
   */
  public function __construct(LoggerChannelFactoryInterface $logger_factory, MessengerInterface $messenger, EntityTypeManagerInterface $entity_type_manager, EditionGuardApiClientInterface $editionguard_api) {
    $this->loggerFactory = $logger_factory;
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity_type_manager;
    $this->editionGuard = $editionguard_api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('logger.factory'), $container->get('messenger'), $container->get('entity_type.manager'), $container->get('editionguard_api.client'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'editionguard_transaction_change_book';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, TransactionInterface $transaction = NULL) {

    $book_id = $transaction->getBookId();
    if (isset($book_id)) {
      $book_storage = $this->entityTypeManager->getStorage('editionguard_book');
      $editionguard_book = $book_storage->load($transaction->getBookId());
    }

    $form['editionguard_transaction_id'] = [
      '#type' => 'value',
      '#default_value' => $transaction->id() ?? NULL,
    ];

    $form['page_title'] = [
      '#type' => 'page_title',
      '#title' => $transaction->label(),
    ];

    $form['book'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'editionguard_book',
      '#title' => $this->t('Books'),
      '#default_value' => $editionguard_book ?? NULL,
      '#multiple' => FALSE,
      '#autocomplete' => TRUE,
      '#required' => TRUE,
    ];

    $form['show_instructions'] = [
      '#type' => 'select',
      '#title' => $this->t('Show Instructions'),
      '#default_value' => 'true',
      '#options' => [
        1 => $this->t('Yes'),
        0 => $this->t('No'),
      ],
    ];

    $form['uses_remaining'] = [
      '#type' => 'number',
      '#title' => $this->t('Uses remaining'),
      '#default_value' => NULL,
    ];

    $form['watermark_enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Watermark - only works on books with DRM EditionMark'),
      '#description' => $this->t('Whether Watermark is allowed on this ebook. Selected user will be used.'),
      '#default_value' => 0,
    ];

    $form['watermark_place_begin'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Place Watermark at start of book'),
      '#default_value' => 0,
      '#states' => [
        'visible' => [
          ':input[name="watermark_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['watermark_place_end'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Place Watermark at end of book'),
      '#default_value' => 0,
      '#states' => [
        'visible' => [
          ':input[name="watermark_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['watermark_place_random'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Place Watermark in random location in book'),
      '#default_value' => 0,
      '#states' => [
        'visible' => [
          ':input[name="watermark_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['watermark_place_random_count'] = [
      '#type' => 'number',
      '#title' => $this->t('Place Watermark in X number of random locations.'),
      '#default_value' => 0,
      '#states' => [
        'visible' => [
          ':input[name="watermark_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->cleanValues()->getValues();
    $book_id = $values['book'];
    $transaction_id = $values['editionguard_transaction_id'];

    if (isset($transaction_id)) {

      /** @var \Drupal\editionguard\Entity\Transaction $transaction */
      $transaction_storage = $this->entityTypeManager->getStorage('editionguard_transaction');
      $transaction = $transaction_storage->load($transaction_id);
      $user = $transaction->getOwner();

      if (isset($book_id)) {

        /** @var \Drupal\editionguard\Entity\Book $editionguard_book */
        $ebook_storage = $this->entityTypeManager->getStorage('editionguard_book');
        $editionguard_book = $ebook_storage->load($book_id);
        $resource_id = $editionguard_book->getResourceId();
        $drm_type = $editionguard_book->getDrmType();

        if (isset($resource_id)) {
          // Get EditionGuard associated ID for transaction.
          $editionguard_transaction_id = $transaction->getTransactionId();
          if (isset($editionguard_transaction_id)) {
            // Delete old transaction.
            $endpoint = $this->editionGuard->getEndpointPluginManager()
              ->createInstance('transaction_delete');

            $delete_query_params = ['transaction_id' => $editionguard_transaction_id];
            $delete_form_params = [];
            $this->editionGuard->request($endpoint, $delete_query_params, $delete_form_params);
          }

          // EditionGuard Api client.
          $form_params = [
            "resource_id" => $resource_id,
            "show_instructions" => $values['show_instructions'],
            "external_id" => $transaction->getExternalId(),
          ];

          if (!empty($values['uses_remaining']) && $values['uses_remaining'] !== '0') {
            $form_params["uses_remaining"] = $values['uses_remaining'];
          }

          // Watermark only works on books with DRM EditionMark.
          if ($drm_type === '3' && $values['watermark_enable'] === 1 && isset($values['user'])) {
            $watermark = $values['watermark_enable'];
            $user_name = $user->label();
            $user_email = $user->getEmail();
            $form_params["watermark_name"] = $user_name ?? '';
            $form_params["watermark_email"] = $user_email ?? '';
            $form_params["watermark_place_begin"] = $values['watermark_place_begin'];
            $form_params["watermark_place_end"] = $values['watermark_place_end'];
            $form_params["watermark_place_random"] = $values['watermark_place_random'];
            $form_params["watermark_place_random_count"] = $values['watermark_place_random_count'];
          }
          $query_params = [];

          // Request new transaction.
          $endpoint = $this->editionGuard->getEndpointPluginManager()
            ->createInstance('transaction_create');
          $request = $this->editionGuard->request($endpoint, $query_params, $form_params);

          if (isset($request['id'])) {

            $transaction->set('transaction_id', $request['id']);
            $transaction->set('book_id', $book_id);
            $transaction->set('resource_id', $request['resource_id']);
            $transaction->set('link', $request['download_link']);
            $transaction->set('show_instructions', $request['show_instructions']);
            $transaction->set('uses_remaining', $values['uses_remaining'] ?? '');
            $transaction->set('watermark', $watermark ?? 0);
            $transaction->save();

            $this->messenger()
              ->addStatus($this->t('The transaction %name has been created.', ['%name' => $transaction->label()]));
            $form_state->setRedirect('entity.editionguard_transaction.collection');

          }
          else {
            $this->loggerFactory->get('editionguard')
              ->error('Transaction Request failed h parameters:' . '<pre><code>' . print_r($form_params, TRUE) . '</code></pre>' . 'Please verify if the EdtionGuard API is working.');
            $this->messenger->addError($this->t('Transaction Request failed. Please review your information and try again.'));
          }
        }
      }
      else {
        $this->loggerFactory->get('editionguard')
          ->error('Transaction Create failed, no book id present');
        $this->messenger->addError($this->t('Transaction Request failed. Please review your information and try again.'));
      }
    }
  }

}
