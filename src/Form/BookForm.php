<?php

namespace Drupal\editionguard\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\editionguard_api\EditionGuardApiClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for EditionGuard book edit forms.
 *
 * @ingroup editionguard
 */
class BookForm extends ContentEntityForm {

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The stream wrapper manager service.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected $streamWrapperManager;

  /**
   * A config object for the system performance configuration.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The EditionGuard API Client.
   *
   * @var \Drupal\editionguard_api\EditionGuardApiClientInterface
   */
  protected $editionGuard;

  /**
   * Constructs a new BookForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A config factory for retrieving required config objects.
   * @param \Drupal\editionguard_api\EditionGuardApiClientInterface $editionguard_api
   *   The EditionGuard client api.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager
   *   The stream wrapper manager service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, LoggerChannelFactoryInterface $logger_factory, ConfigFactoryInterface $config_factory, EditionGuardApiClientInterface $editionguard_api, StreamWrapperManagerInterface $stream_wrapper_manager) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);

    $this->loggerFactory = $logger_factory;
    $this->config = $config_factory->get('editionguard_book.settings');
    $this->editionGuard = $editionguard_api;
    $this->streamWrapperManager = $stream_wrapper_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('logger.factory'),
      $container->get('config.factory'),
      $container->get('editionguard_api.client'),
      $container->get('stream_wrapper_manager')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\editionguard\Entity\Book $editionguard_book */
    $editionguard_book = $this->entity;
    $form = parent::buildForm($form, $form_state);

    // Disable DRM Type on edit. Changing DRM can break existing transactions.
    if (!$editionguard_book->isNew()) {
      $form['drm']['#disabled'] = TRUE;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    /** @var \Drupal\editionguard\Entity\Book $editionguard_book */
    $editionguard_book = $this->entity;
    $form_params = [];

    // Get file realpath.
    $fid = $editionguard_book->get("resource")->entity->getFileUri();
    $file_path = $this->streamWrapperManager->getViaUri($fid)->realpath();
    $form_params['resource'] = $file_path;

    // Get book settings configuration.
    $book_prefix = $this->config->get('book_prefix');

    // Create required parameters.
    $fields = ['title', 'author', 'publisher', 'drm'];

    foreach ($fields as $field) {
      if (isset($editionguard_book->get($field)->getValue()[0]['value'])) {
        $field_value = $editionguard_book->get($field)->getValue()[0]['value'];
        if (isset($field_value)) {
          if ($field === 'title') {
            $form_params[$field] = $book_prefix . $field_value;
          }
          else {
            $form_params[$field] = $field_value;
          }
        }
      }
    }
    if ($editionguard_book->isNew()) {

      // Use book_create if entity is new.
      $endpoint = $this->editionGuard->getEndpointPluginManager()
        ->createInstance('book_create');
      $result = $this->editionGuard->request($endpoint, $query_params = [], $form_params);

      // Set book ID and UUID from EditionGuard API.
      if (isset($result['id']) && isset($result['resource_id'])) {
        $editionguard_book->set('editionguard_book_id', $result['id']);
        $editionguard_book->set('editionguard_resource_id', $result['resource_id']);

        $this->messenger()
          ->addMessage($this->t('Book successfully added on EditionGuard.'));
      }
      else {
        $this->messenger()
          ->addMessage($this->t('Something went wrong, please check the logs for more details.'));
      }
    }
    else {
      $book_id = $editionguard_book->getBookId();
      if (isset($book_id)) {
        $endpoint = $this->editionGuard->getEndpointPluginManager()
          ->createInstance('book_replace');
        $query_params = ['id' => $book_id];
        $result = $this->editionGuard->request($endpoint, $query_params, $form_params);

        // Set UUID from EditionGuard API.
        if (isset($result['resource_id'])) {

          $this->messenger()
            ->addMessage($this->t('Book successfully updated on EditionGuard.'));

          if ($editionguard_book->get('editionguard_resource_id')
            ->getValue()[0]['value'] !== $result['resource_id']) {
            $editionguard_book->set('editionguard_resource_id', $result['resource_id']);

            $this->messenger()
              ->addMessage($this->t('The book UUID has been changed.'));

            $this->loggerFactory->get('editionguard')
              ->info('@type: The book UUID has been changed for %title. Old UUID: @old , New UUID: @new .', [
                '@type' => $editionguard_book->bundle(),
                '%title' => $editionguard_book->label(),
                '@old' => $editionguard_book->get('editionguard_resource_id')
                  ->getValue(),
                '@new' => $result['resource_id'],
              ]);
          }

        }
        else {
          $this->messenger()
            ->addMessage($this->t('Something went wrong, please check the logs for more details.'));
        }
      }
      else {
        $this->loggerFactory->get('editionguard')
          ->warning('@type: Could not find any ebook id for %title.', [
            '@type' => $editionguard_book->bundle(),
            '%title' => $editionguard_book->label(),
          ]);
      }
    }

    $status = parent::save($form, $form_state);
    switch ($status) {
      case SAVED_NEW:
        $this->messenger()
          ->addMessage($this->t('Created the %label EditionGuard book.', [
            '%label' => $editionguard_book->label(),
          ]));
        break;

      default:
        $this->messenger()
          ->addMessage($this->t('Saved the %label EditionGuard book.', [
            '%label' => $editionguard_book->label(),
          ]));
    }
    $form_state->setRedirect('entity.editionguard_book.collection');
  }

}
