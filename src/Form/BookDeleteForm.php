<?php

namespace Drupal\editionguard\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\editionguard_api\EditionGuardApiClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting EditionGuard book entities.
 *
 * @ingroup editionguard
 */
class BookDeleteForm extends ContentEntityDeleteForm {

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The stream wrapper manager service.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected $streamWrapperManager;

  /**
   * The EditionGuard API Client.
   *
   * @var \Drupal\editionguard_api\EditionGuardApiClientInterface
   */
  protected $editionGuard;

  /**
   * Constructs a new BookDelete object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\editionguard_api\EditionGuardApiClientInterface $editionguard_api
   *   The EditionGuard client api.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager
   *   The stream wrapper manager service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, LoggerChannelFactoryInterface $logger_factory, MessengerInterface $messenger, EditionGuardApiClientInterface $editionguard_api, StreamWrapperManagerInterface $stream_wrapper_manager) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);

    $this->loggerFactory = $logger_factory;
    $this->messenger = $messenger;
    $this->streamWrapperManager = $stream_wrapper_manager;
    $this->editionGuard = $editionguard_api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity.repository'), $container->get('entity_type.bundle.info'), $container->get('datetime.time'), $container->get('logger.factory'), $container->get('messenger'), $container->get('editionguard_api.client'), $container->get('stream_wrapper_manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $editionguard_book = $this->getEntity();

    // Check if there are transactions associated with the book.
    $transaction_ids = \Drupal::entityQuery('editionguard_transaction')
      ->condition('book_id', $editionguard_book->id())
      ->condition('is_fulfilled', 0)
      ->execute();

    if (!$transaction_ids) {
      return parent::buildForm($form, $form_state);
    }

    $transactions = $this->entityManager->getStorage('editionguard_transaction')
      ->loadMultiple($transaction_ids);
    $form['transactions'] = [
      '#type' => 'value',
      '#default_value' => $transactions,
    ];

    $header = [
      'external_id' => [
        'data' => $this->t('External ID'),
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      'name' => [
        'data' => $this->t('Name'),
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
      'book' => [
        'data' => $this->t('book'),
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
      'owner' => [
        'data' => $this->t('Owner'),
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      'created' => [
        'data' => $this->t('Created'),
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
    ];

    foreach ($transactions as $transaction) {

      $row = [
        'external_id' => $transaction->getExternalId(),
        'name' => $transaction->label(),
        'book' => $editionguard_book->label(),
        'owner' => [
          'data' => [
            '#theme' => 'username',
            '#account' => $transaction->getOwner(),
          ],
        ],
        'created' => \Drupal::service('date.formatter')
          ->format($transaction->getCreatedTime(), 'short'),
      ];

      $rows[] = $row;
    }
    $form['transactions_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No unfulfilled transactions.'),
      '#weight' => 1,
    ];

    $form['book'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'editionguard_book',
      '#title' => $this->t('Books'),
      '#default_value' => $editionguard_book ?? NULL,
      '#multiple' => FALSE,
      '#autocomplete' => TRUE,
      '#required' => TRUE,
    ];

    $form['show_instructions'] = [
      '#type' => 'select',
      '#title' => $this->t('Show Instructions'),
      '#default_value' => 'true',
      '#options' => [
        1 => $this->t('Yes'),
        0 => $this->t('No'),
      ],
    ];

    $form['uses_remaining'] = [
      '#type' => 'number',
      '#title' => $this->t('Uses remaining'),
      '#default_value' => NULL,
    ];

    $form['watermark_enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Watermark - only works on books with DRM EditionMark'),
      '#description' => $this->t('Whether Watermark is allowed on this ebook. Selected user will be used.'),
      '#default_value' => 0,
    ];

    $form['watermark_place_begin'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Place Watermark at start of book'),
      '#default_value' => 0,
      '#states' => [
        'visible' => [
          ':input[name="watermark_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['watermark_place_end'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Place Watermark at end of book'),
      '#default_value' => 0,
      '#states' => [
        'visible' => [
          ':input[name="watermark_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['watermark_place_random'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Place Watermark in random location in book'),
      '#default_value' => 0,
      '#states' => [
        'visible' => [
          ':input[name="watermark_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['watermark_place_random_count'] = [
      '#type' => 'number',
      '#title' => $this->t('Place Watermark in X number of random locations.'),
      '#default_value' => 0,
      '#states' => [
        'visible' => [
          ':input[name="watermark_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Allocate transactions to a different book'),
      '#submit' => ['::changeBook'],
    ];

    if ($transaction_ids) {
      $form['cannot_delete'] = [
        '#markup' => $this->t('You cannot delete this book because it is currently associated with one or more unfulfilled transactions. You should transfer the transactions to a different book or wait for them to be fulfilled before trying again.'),
        '#weight' => 2,
      ];

      return $form;
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $book = $this->getEntity();
    $book_id = $book->getBookId();

    if (isset($book_id)) {
      // Edition Guard Client API.
      $client = \Drupal::service('editionguard_api.client');

      // Use book_create if entity is new.
      $endpoint = $client->getEndpointPluginManager()
        ->createInstance('book_delete');

      $query_params = ['id' => $book_id];
      $form_params = [];
      $client->request($endpoint, $query_params, $form_params);

      $this->messenger()
        ->addMessage($this->t('Book successfully deleted on EditionGuard.'));
    }

    $book->delete();
    $this->messenger()->addMessage($this->getDeletionMessage());
    $this->logDeletionMessage();
    $form_state->setRedirectUrl($this->getRedirectUrl());
  }

  /**
   * Submit callback for the "Allocate transactions to a different book" button.
   */
  public function changeBook(array &$form, FormStateInterface $form_state) {
    $values = $form_state->cleanValues()->getValues();
    $book_id = $values['book'];
    foreach ($values['transactions'] as $transaction) {

      $user = $transaction->getOwner();

      if (isset($book_id)) {

        /** @var \Drupal\editionguard\Entity\Book $editionguard_book */
        $ebook_storage = $this->entityTypeManager->getStorage('editionguard_book');
        $editionguard_book = $ebook_storage->load($book_id);
        $resource_id = $editionguard_book->getResourceId();
        $drm_type = $editionguard_book->getDrmType();

        if (isset($resource_id)) {
          // Get EditionGuard associated ID for transaction.
          $editionguard_transaction_id = $transaction->getTransactionId();
          if (isset($editionguard_transaction_id)) {
            // Delete old transaction.
            $endpoint = $this->editionGuard->getEndpointPluginManager()
              ->createInstance('transaction_delete');

            $delete_query_params = ['transaction_id' => $editionguard_transaction_id];
            $delete_form_params = [];
            $this->editionGuard->request($endpoint, $delete_query_params, $delete_form_params);
          }

          // EditionGuard Api client.
          $form_params = [
            "resource_id" => $resource_id,
            "show_instructions" => $values['show_instructions'],
            "external_id" => $transaction->getExternalId(),
          ];

          if (!empty($values['uses_remaining']) && $values['uses_remaining'] !== '0') {
            $form_params["uses_remaining"] = $values['uses_remaining'];
          }

          // Watermark only works on books with DRM EditionMark.
          if ($drm_type === '3' && $values['watermark_enable'] === 1 && isset($values['user'])) {
            $watermark = $values['watermark_enable'];
            $user_name = $user->label();
            $user_email = $user->getEmail();
            $form_params["watermark_name"] = $user_name ?? '';
            $form_params["watermark_email"] = $user_email ?? '';
            $form_params["watermark_place_begin"] = $values['watermark_place_begin'];
            $form_params["watermark_place_end"] = $values['watermark_place_end'];
            $form_params["watermark_place_random"] = $values['watermark_place_random'];
            $form_params["watermark_place_random_count"] = $values['watermark_place_random_count'];
          }
          $query_params = [];

          // Request new transaction.
          $endpoint = $this->editionGuard->getEndpointPluginManager()
            ->createInstance('transaction_create');
          $request = $this->editionGuard->request($endpoint, $query_params, $form_params);

          if (isset($request['id'])) {

            $transaction->set('transaction_id', $request['id']);
            $transaction->set('book_id', $book_id);
            $transaction->set('resource_id', $request['resource_id']);
            $transaction->set('link', $request['download_link']);
            $transaction->set('show_instructions', $request['show_instructions']);
            $transaction->set('uses_remaining', $values['uses_remaining'] ?? '');
            $transaction->set('watermark', $watermark ?? 0);
            $transaction->save();

            $this->messenger()
              ->addStatus($this->t('The transaction %name has been updated.', ['%name' => $transaction->label()]));
            $form_state->setRedirect('entity.editionguard_transaction.collection');

          }
          else {
            $this->loggerFactory->get('editionguard')
              ->error('Transaction Request failed h parameters:' . '<pre><code>' . print_r($form_params, TRUE) . '</code></pre>' . 'Please verify if the EdtionGuard API is working.');
            $this->messenger->addError($this->t('Transaction Request failed. Please review your information and try again.'));
          }
        }
      }
      else {
        $this->loggerFactory->get('editionguard')
          ->error('Transaction Create failed, no book id present');
        $this->messenger->addError($this->t('Transaction Request failed. Please review your information and try again.'));
      }
    }

    $form_state->setRebuild();
  }

}
