<?php

namespace Drupal\editionguard\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\editionguard\Entity\TransactionInterface;
use Drupal\editionguard_api\EditionGuardApiClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * An example controller.
 */
class TransactionRegenerateController extends ControllerBase {

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The EditionGuard API Client.
   *
   * @var \Drupal\editionguard_api\EditionGuardApiClientInterface
   */
  protected $editionGuard;

  /**
   * Constructs a new EditionGuard object.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\editionguard_api\EditionGuardApiClientInterface $editionguard_api
   *   The EditionGuard client api.
   */
  public function __construct(LoggerChannelFactoryInterface $logger_factory, MessengerInterface $messenger, EntityTypeManagerInterface $entity_type_manager, EditionGuardApiClientInterface $editionguard_api) {
    $this->loggerFactory = $logger_factory;
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity_type_manager;
    $this->editionGuard = $editionguard_api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('logger.factory'), $container->get('messenger'), $container->get('entity_type.manager'), $container->get('editionguard_api.client'));
  }

  /**
   * Recreate transaction.
   *
   * @param \Drupal\editionguard\Entity\TransactionInterface $transaction
   *   The EditionGuard transaction entity.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirection to transaction collection page.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function regenerateTransaction(TransactionInterface $transaction) {

    $book_id = $transaction->getBookId();
    $user = $transaction->getOwner();

    if (isset($book_id)) {

      /** @var \Drupal\editionguard\Entity\Book $editionguard_book */
      $ebook_storage = $this->entityTypeManager->getStorage('editionguard_book');
      $editionguard_book = $ebook_storage->load($book_id);
      $resource_id = $editionguard_book->getResourceId();
      $drm_type = $editionguard_book->getDrmType();

      if (isset($resource_id)) {
        // Get EditionGuard associated ID for transaction.
        $editionguard_transaction_id = $transaction->getTransactionId();
        if (isset($editionguard_transaction_id)) {
          // Delete old transaction.
          $endpoint = $this->editionGuard->getEndpointPluginManager()
            ->createInstance('transaction_delete');

          $delete_query_params = ['transaction_id' => $editionguard_transaction_id];
          $delete_form_params = [];
          $this->editionGuard->request($endpoint, $delete_query_params, $delete_form_params);
        }

        // EditionGuard Api client.
        $form_params = [
          "resource_id" => $resource_id,
          "show_instructions" => $transaction->getShowInstructions(),
          "external_id" => $transaction->getExternalId(),
        ];

        $uses_remaining = $transaction->getUsesRemaining();
        if (!empty($uses_remaining) && $uses_remaining !== '0') {
          $form_params["uses_remaining"] = $uses_remaining;
        }

        $watermark = $transaction->getWatermark();
        // Watermark only works on books with DRM EditionMark.
        if ($drm_type === '3' && $watermark === 1 && isset($values['user'])) {
          $user_name = $user->label();
          $user_email = $user->getEmail();
          $form_params["watermark_name"] = $user_name ?? '';
          $form_params["watermark_email"] = $user_email ?? '';
          $form_params["watermark_place_begin"] = '1';
          $form_params["watermark_place_end"] = '1';
          $form_params["watermark_place_random"] = '1';
          $form_params["watermark_place_random_count"] = '5';
        }
        $query_params = [];

        // Request new transaction.
        $endpoint = $this->editionGuard->getEndpointPluginManager()
          ->createInstance('transaction_create');
        $request = $this->editionGuard->request($endpoint, $query_params, $form_params);

        if (isset($request['id'])) {

          $transaction->set('transaction_id', $request['id']);
          $transaction->set('resource_id', $request['resource_id']);
          $transaction->set('link', $request['download_link']);
          $transaction->set('show_instructions', $request['show_instructions']);
          $transaction->save();
          $this->messenger()
            ->addStatus($this->t('The transaction %name has been regenerated.', ['%name' => $transaction->label()]));

        }
        else {
          $this->loggerFactory->get('editionguard')
            ->error('Transaction Request failed h parameters:' . '<pre><code>' . print_r($form_params, TRUE) . '</code></pre>' . 'Please verify if the EdtionGuard API is working.');
          $this->messenger->addError($this->t('Transaction Request failed. Please review your information and try again.'));
        }
      }
    }
    else {
      $this->loggerFactory->get('editionguard')
        ->error('Transaction Create failed, no book id present');
      $this->messenger->addError($this->t('Transaction Request failed. Please review your information and try again.'));
    }

    return $this->redirect('entity.editionguard_transaction.collection');
  }

}
