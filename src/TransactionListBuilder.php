<?php

namespace Drupal\editionguard;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\commerce_order\Entity\Order;
use Drupal\editionguard\Entity\Book;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of EditionGuard Transaction entities.
 *
 * @ingroup editionguard
 */
class TransactionListBuilder extends EntityListBuilder {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a new TransactionListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler, DateFormatterInterface $date_formatter) {
    parent::__construct($entity_type, $entity_type_manager->getStorage($entity_type->id()));
    $this->moduleHandler = $module_handler;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static($entity_type, $container->get('entity_type.manager'), $container->get('module_handler'), $container->get('date.formatter'));
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'id' => [
        'data' => $this->t('ID'),
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      'external_id' => [
        'data' => $this->t('External ID'),
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      'transaction_id' => [
        'data' => $this->t('Transaction ID'),
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      'name' => [
        'data' => $this->t('Name'),
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
      'book_id' => [
        'data' => $this->t('book'),
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
      'owner' => [
        'data' => $this->t('Owner'),
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      'created' => [
        'data' => $this->t('Created'),
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\editionguard\Entity\Transaction $entity */
    $external_id = $entity->getExternalId();
    $name = $entity->label();
    $book_id = $entity->getBookId();

    if (isset($book_id)) {
      /** @var \Drupal\editionguard\Entity\Book $book */
      $book = Book::load($book_id);
      $book_id = Link::createFromRoute($book->label(), 'entity.editionguard_book.edit_form', ['editionguard_book' => $book->id()]);
    }

    if ($this->moduleHandler->moduleExists('commerce_order')) {
      if (isset($external_id)) {
        /** @var \Drupal\commerce_order\Entity\Order $order */
        $order = Order::load($external_id);
        if (isset($order)) {
          $external_id = Link::createFromRoute('Order (' . $order->id() . ')', 'entity.commerce_order.edit_form', ['commerce_order' => $order->id()]);
        }
      }
    }

    $row = [
      'id' => $entity->id(),
      'external_id' => $external_id,
      'transaction_id' => $entity->getTransactionId(),
      'name' => $name,
      'book_id' => $book_id,
      'owner' => [
        'data' => [
          '#theme' => 'username',
          '#account' => $entity->getOwner(),
        ],
      ],
      'created' => $this->dateFormatter->format($entity->getCreatedTime(), 'short'),
    ];

    return $row + parent::buildRow($entity);
  }

}
